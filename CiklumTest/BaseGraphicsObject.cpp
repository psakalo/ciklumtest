//
//  BaseGraphicsObject.cpp
//  CiklumTest
//
//  Created by Pavel Sakalo on 04.03.15.
//  Copyright (c) 2015 Pavel Sakalo. All rights reserved.
//

#include "BaseGraphicsObject.h"
#include "FrameRenderer.h"

BaseGraphicsObject::BaseGraphicsObject()
{
    
}

BaseGraphicsObject::~BaseGraphicsObject()
{
    
}

void BaseGraphicsObject::putCharacterToBuffer(char* frameBuffer, int x, int y, char c)
{
    if (x >= 0 && x < FrameRenderer::FRAME_WIDTH && y >= 0 && y < FrameRenderer::FRAME_HEIGHT)
        frameBuffer[y * FrameRenderer::FRAME_WIDTH + x] = c;
}