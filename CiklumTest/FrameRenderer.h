//
//  FrameRenderer.h
//  CiklumTest
//
//  Created by Pavel Sakalo on 04.03.15.
//  Copyright (c) 2015 Pavel Sakalo. All rights reserved.
//

#ifndef __CiklumTest__FrameRenderer__
#define __CiklumTest__FrameRenderer__

#include <stdio.h>
#include <ostream>
#include <vector>
#include <memory>

class BaseGraphicsObject;

class FrameRenderer
{
public:
    
    static const int FRAME_WIDTH = 20;
    static const int FRAME_HEIGHT = 10;
    static const char DEFAULT_CHARACTER = ' ';
    
    FrameRenderer();
    ~FrameRenderer();
    
    // Draws all objects from _graphicObjects to _frameBuffer
    void render();
    // Fills _frameBuffer with DEFAULT_CHARACTER
    void clear();
    
    // You can pass cout or any fstream here, so you can also render to file
    void print(std::ostream& st);

    void addGraphicsObject(const std::shared_ptr<BaseGraphicsObject>& obj);
    bool addGraphicsObjectsFromString(const std::string& str);
    
protected:
    
    char* _frameBuffer;
    std::vector<std::shared_ptr<BaseGraphicsObject> > _graphicObjects;
    
};

#endif /* defined(__CiklumTest__FrameRenderer__) */
