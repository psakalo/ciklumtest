//
//  SmartLineGraphicsObject.h
//  CiklumTest
//
//  Created by Pavel Sakalo on 04.03.15.
//  Copyright (c) 2015 Pavel Sakalo. All rights reserved.
//

#ifndef __CiklumTest__SmartLineGraphicsObject__
#define __CiklumTest__SmartLineGraphicsObject__

#include "LineGraphicsObject.h"

class SmartLineGraphicsObject : public LineGraphicsObject
{
public:
    
    SmartLineGraphicsObject(int x1, int y1, int x2, int y2);
    virtual ~SmartLineGraphicsObject();
    
protected:
    
    virtual void putCharacterToBuffer(char* frameBuffer, int x, int y, char c);
};

typedef std::shared_ptr<SmartLineGraphicsObject> SmartLineGraphicsObjectPtr;

#endif /* defined(__CiklumTest__SmartLineGraphicsObject__) */
