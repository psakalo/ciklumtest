//
//  main.cpp
//  CiklumTest
//
//  Created by Pavel Sakalo on 04.03.15.
//  Copyright (c) 2015 Pavel Sakalo. All rights reserved.
//

#include <iostream>
#include "FrameRenderer.h"
#include "LineGraphicsObject.h"

int main(int argc, const char * argv[])
{
    if (argc > 1)
    {
        FrameRenderer fr;
    
        fr.addGraphicsObjectsFromString(argv[1]);
        fr.render();
        fr.print(std::cout);
    }
    else
    {
        std::cout << "No input data." << std::endl;
    }
    
    return 0;
}
