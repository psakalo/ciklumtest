//
//  FrameRenderer.cpp
//  CiklumTest
//
//  Created by Pavel Sakalo on 04.03.15.
//  Copyright (c) 2015 Pavel Sakalo. All rights reserved.
//

#include "FrameRenderer.h"
#include "SmartLineGraphicsObject.h"
#include <regex>

FrameRenderer::FrameRenderer()
{
    _frameBuffer = new char[FRAME_WIDTH * FRAME_HEIGHT];
    clear();
}

FrameRenderer::~FrameRenderer()
{
    delete [] _frameBuffer;
}

void FrameRenderer::render()
{
    for (auto obj : _graphicObjects)
    {
        if (obj)
            obj->draw(_frameBuffer);
    }
}

void FrameRenderer::clear()
{
    if (_frameBuffer)
        memset(_frameBuffer, DEFAULT_CHARACTER, sizeof(char) * FRAME_WIDTH * FRAME_HEIGHT);
}

void FrameRenderer::print(std::ostream& st)
{
    if (_frameBuffer)
    {
        for (int i = 0; i < FRAME_HEIGHT; ++i)
        {
            st.write(_frameBuffer + i * FRAME_WIDTH, FRAME_WIDTH);
            st << std::endl;
        }
    }
}

void FrameRenderer::addGraphicsObject(const std::shared_ptr<BaseGraphicsObject> &obj)
{
    _graphicObjects.push_back(obj);
}

bool FrameRenderer::addGraphicsObjectsFromString(const std::string& str)
{
    std::smatch sm;
    std::regex e("\\s*(\\((\\d+),(\\d+)\\)\\s*(–|-)\\s*\\((\\d+),(\\d+)\\))(,|$)");
    std::string s(str);
    
    while (std::regex_search(s, sm, e))
    {
        int x1 = std::stoi(sm[2].str());
        int y1 = std::stoi(sm[3].str());
        int x2 = std::stoi(sm[5].str());
        int y2 = std::stoi(sm[6].str());
        addGraphicsObject(SmartLineGraphicsObjectPtr(new SmartLineGraphicsObject(x1, y1, x2, y2)));
        s = sm.suffix().str();
    }
    
    return true;
}