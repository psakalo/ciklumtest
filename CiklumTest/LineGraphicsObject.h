//
//  LineGraphicsObject.h
//  CiklumTest
//
//  Created by Pavel Sakalo on 04.03.15.
//  Copyright (c) 2015 Pavel Sakalo. All rights reserved.
//

#ifndef __CiklumTest__LineGraphicsObject__
#define __CiklumTest__LineGraphicsObject__

#include "BaseGraphicsObject.h"
#include <memory>

class LineGraphicsObject : public BaseGraphicsObject
{
public:
    
    LineGraphicsObject(int x1, int y1, int x2, int y2, char c);
    virtual ~LineGraphicsObject();
    
    virtual void draw(char* frameBuffer);
    
protected:
    
    int inline sign(int v);
    
    int _x1, _y1, _x2, _y2;
    char _c;
};

typedef std::shared_ptr<LineGraphicsObject> LineGraphicsObjectPtr;

#endif /* defined(__CiklumTest__LineGraphicsObject__) */
