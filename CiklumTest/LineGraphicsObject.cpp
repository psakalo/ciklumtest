//
//  LineGraphicsObject.cpp
//  CiklumTest
//
//  Created by Pavel Sakalo on 04.03.15.
//  Copyright (c) 2015 Pavel Sakalo. All rights reserved.
//

#include "LineGraphicsObject.h"
#include "FrameRenderer.h"
#include <math.h>

LineGraphicsObject::LineGraphicsObject(int x1, int y1, int x2, int y2, char c)
    : _x1(x1)
    , _y1(y1)
    , _x2(x2)
    , _y2(y2)
    , _c(c)
{
    
}

LineGraphicsObject::~LineGraphicsObject()
{
    
}

void LineGraphicsObject::draw(char *frameBuffer)
{
    int x = _x1;
    int y = _y1;
    int dx = abs(_x2 - _x1);
    int dy = abs(_y2 - _y1);
    int s1 = sign(_x2 - _x1);
    int s2 = sign(_y2 - _y1);
    
    bool change = false;
    if (dy > dx)
    {
        int tmp = dx;
        dx = dy;
        dy = tmp;
        change = true;
    }
    
    int t = 2 * dy - dx;
    for (int i = 0; i <= dx; ++i)
    {
        putCharacterToBuffer(frameBuffer, x, y, _c);
        while (t >= 0)
        {
            if (change)
                x += s1;
            else
                y += s2;
            t = t - 2 * dx;
        }
        if (change)
            y += s2;
        else
            x += s1;
        t = t + 2 * dy;
    }
}

int LineGraphicsObject::sign(int v)
{
    return (v > 0) - (v < 0);
}