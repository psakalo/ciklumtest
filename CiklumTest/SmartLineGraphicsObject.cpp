//
//  SmartLineGraphicsObject.cpp
//  CiklumTest
//
//  Created by Pavel Sakalo on 04.03.15.
//  Copyright (c) 2015 Pavel Sakalo. All rights reserved.
//

#include "SmartLineGraphicsObject.h"
#include "FrameRenderer.h"

SmartLineGraphicsObject::SmartLineGraphicsObject(int x1, int y1, int x2, int y2)
    : LineGraphicsObject(x1, y1, x2, y2, '*')
{
    if (x1 == x2)
        _c = '|';
    else if (y1 == y2)
        _c = '-';
    else if ((x2 > x1 && y2 > y1) || (x1 > x2 && y1 > y2))
        _c = '\\';
    else
        _c = '/';
}

SmartLineGraphicsObject::~SmartLineGraphicsObject()
{
    
}

void SmartLineGraphicsObject::putCharacterToBuffer(char *frameBuffer, int x, int y, char c)
{
    if (x >= 0 && x < FrameRenderer::FRAME_WIDTH && y >= 0 && y < FrameRenderer::FRAME_HEIGHT)
    {
        char c = _c;
        if (frameBuffer[y * FrameRenderer::FRAME_WIDTH + x] != FrameRenderer::DEFAULT_CHARACTER && frameBuffer[y * FrameRenderer::FRAME_WIDTH + x] != _c)
            c = '*';
        
        frameBuffer[y * FrameRenderer::FRAME_WIDTH + x] = c;
    }
}