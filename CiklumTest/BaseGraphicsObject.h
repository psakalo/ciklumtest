//
//  BaseGraphicsObject.h
//  CiklumTest
//
//  Created by Pavel Sakalo on 04.03.15.
//  Copyright (c) 2015 Pavel Sakalo. All rights reserved.
//

#ifndef __CiklumTest__BaseGraphicsObject__
#define __CiklumTest__BaseGraphicsObject__

#include <stdio.h>

class BaseGraphicsObject
{
public:
    
    BaseGraphicsObject();
    virtual ~BaseGraphicsObject();
    
    virtual void draw(char* frameBuffer) = 0;
    
protected:
    
    virtual void putCharacterToBuffer(char* frameBuffer, int x, int y, char c);
};

#endif /* defined(__CiklumTest__BaseGraphicsObject__) */
